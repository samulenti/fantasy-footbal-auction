<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PublisherController
 * @package App\Controller
 */
class PublisherController extends AbstractController
{
    /**
	 * Handles a publish request.
	 *
     * @Route("/publish/{topic}", name="publisher", methods={"POST"})
	 *
	 * @param Publisher $publisher
	 * @param string $topic
	 * @param Request $request
	 *
	 * @return Response
     */
    public function index(Publisher $publisher, string $topic, Request $request)
    {
        $update = new Update($topic, $request->getContent());
        $publisher($update);

        return new Response('success');
    }
}
