<?php

namespace App\Controller;

use App\Entity\Group;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class GroupsController extends AbstractFOSRestController
{
	/**
	 * Gets the list of Group resources.
	 *
	 * @Rest\Get("/groups")
	 *
	 * @return Response
	 */
	public function getGroups(): Response
	{
		$repository = $this->getDoctrine()->getRepository(Group::class);
		$movies = $repository->findall();

		return $this->handleView($this->view($movies));
	}

	/**
	 * Gets the a single Group resource by id (if any).
	 *
	 * @Rest\Get("/groups/{id}")
	 *
	 * @param int $id
	 * @return Response
	 */
	public function getGroup(int $id): ?Response
	{
		$repository = $this->getDoctrine()->getRepository(Group::class);
		$group = $repository->find($id);

		if (!$group) {
			throw $this->createNotFoundException(
				'No group found for id '.$id
			);
		}

		return $this->handleView($this->view($group));
	}

	/**
	 * Creates an Group resource.
	 *
	 * @Rest\Post("/groups")
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function postGroup(Request $request): Response
	{
		$group = new Group();

		$group->setName($request->get('name'));
		$group->setAccessCode($request->get('accessCode'));

		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($group);
		$entityManager->flush();

		return $this->handleView($this->view([ 'status' => 'ok' ],Response::HTTP_CREATED));
	}

}
